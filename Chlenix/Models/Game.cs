﻿namespace Chlenix.Models
{
    internal class Game
    {
        public string Value { get; }
        public string WindowTitle { get; }
        public Script AvailableScrtips { get; }

        public Game(string value, string windowTitle, Script availableScrtips)
        {
            Value = value;
            WindowTitle = windowTitle;
            AvailableScrtips = availableScrtips;
        }
    }
}

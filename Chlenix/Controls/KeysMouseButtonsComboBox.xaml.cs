﻿using System.ComponentModel;
using System.Collections.Generic;
using System.Runtime.CompilerServices;

using static Chlenix.Utils.KeyMouseHelper;

namespace Chlenix.Controls
{
    public sealed partial class KeysMouseButtonsComboBox : INotifyPropertyChanged
    {
        // ReSharper disable once MemberCanBePrivate.Global
        public IEnumerable<Key> Keys { get; } = new List<Key>
        {
            Key.Space,
            Key.LeftAlt,
            Key.XButton1,
            Key.XButton2,
            Key.N
        };

        private Key _selectedKey;
        public Key SelectedKey
        {
            get { return _selectedKey; }
            // ReSharper disable once UnusedMember.Global
            set
            {
                _selectedKey = value;
                SelectedKeyPropertyChanged();
            }
        }

        public KeysMouseButtonsComboBox()
        {
            InitializeComponent();
            DataContext = this;
        }

        public event PropertyChangedEventHandler PropertyChanged;

        private void SelectedKeyPropertyChanged([CallerMemberName] string propertyName = null)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }
    }
}

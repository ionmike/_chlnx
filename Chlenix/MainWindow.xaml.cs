﻿﻿using System;
using System.Linq;
using System.Windows;
using System.Threading;
using System.Reflection;
using System.Diagnostics;
using System.ComponentModel;
using System.Windows.Controls;
using System.Windows.Threading;
using System.Collections.Generic;
using System.Runtime.InteropServices;
using System.Runtime.CompilerServices;

using Chlenix.Utils;
using Chlenix.Models;
using static Chlenix.Utils.KeyMouseHelper;

namespace Chlenix
{
    [Flags]
    internal enum Script
    {
        Bhop = 1 << 0,
        JumpRock = 1 << 1,
        CrazyMouse = 1 << 2
    }

    public sealed partial class MainWindow : INotifyPropertyChanged
    {
        #region Fields and Props

        private IntPtr _hWindow;

        private readonly DispatcherTimer _gameListenerDispatcherTimer = new DispatcherTimer();

        private readonly Dictionary<Script, LoopingThread> _threads;

        private readonly List<Game> _games = new List<Game>
        {
            new Game("Left4Dead2", "Left 4 Dead 2", Script.Bhop | Script.JumpRock | Script.CrazyMouse),
            new Game("TeamFortress2", "Team Fortress 2", Script.Bhop | Script.CrazyMouse)
        };

        private string _gameWindowTitle;

        private readonly Random _random = new Random();

        // ReSharper disable once AssignNullToNotNullAttribute
        private static readonly FileVersionInfo VersionInfo = 
            FileVersionInfo.GetVersionInfo(Assembly.GetEntryAssembly().Location);
        private static readonly string Copyright = 
            $"{VersionInfo.ProductName} v{VersionInfo.ProductVersion.Substring(0, 3)} by {VersionInfo.CompanyName}";

        private string _statusBarText = Copyright;
        // ReSharper disable once MemberCanBePrivate.Global
        public string StatusBarText
        {
            // ReSharper disable once UnusedMember.Global
            get { return _statusBarText; }
            private set
            {
                _statusBarText = value;
                OnPropertyChanged();
            }
        }

        #endregion

        public MainWindow()
        {
            DataContext = this;
            InitializeComponent();

            try
            {
                _threads = new Dictionary<Script, LoopingThread>
                {
                    { Script.Bhop, new LoopingThread(BhopThread) },
                    { Script.JumpRock, new LoopingThread(JumpRockThread) },
                    { Script.CrazyMouse, new LoopingThread(CrazyMouseThread) }
                };
            }
            catch (ArgumentNullException e)
            {
                MessageBox.Show(e.Message);
            }

            try
            {
                _gameListenerDispatcherTimer.Tick += GameListenerDispatcherTimerOnTick;
                _gameListenerDispatcherTimer.Interval = TimeSpan.FromSeconds(5);
                _gameListenerDispatcherTimer.Start();

                _threads.Values.ToList().ForEach(t => { t.Start(); t.Pause(); });
            }
            catch (NullReferenceException e)
            {
                MessageBox.Show(e.Message);
            }

            // NOTE we manually redraw controls on initialization by call method
            RedrawControls(_games[0]);
        }

        /// <summary>
        /// Listener method of game launch status.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="eventArgs"></param>
        private void GameListenerDispatcherTimerOnTick(object sender, EventArgs eventArgs)
        {
            _hWindow = FindWindow(null, _gameWindowTitle);

            if (_hWindow == IntPtr.Zero)
            {
                _threads.Values.ToList().ForEach(t => t.Pause());
                StatusBarText = Copyright;
                return;
            }

            // Find game and start script threads if game has available script.
            Game game = _games.FirstOrDefault(x => x.WindowTitle == _gameWindowTitle);

            // ReSharper disable once PossibleNullReferenceException
            _threads.Where(t => game.AvailableScrtips.HasFlag(t.Key)).ToList().ForEach(t => t.Value.Resume());

            StatusBarText = _gameWindowTitle + " window found!";
        }

        #region Threads

        /// <summary>
        /// Thread for Bunny Hop.
        /// Spam space if key pressed.
        /// </summary>
        private void BhopThread()
        {
            if (IsKeyPressed(BhopKeyComboBox.SelectedKey))
            {
                Thread.Sleep(11);
                SendMessage(_hWindow, (int) KeyState.KeyDown, (IntPtr) Key.Space, (IntPtr) 0x390000);
                Thread.Sleep(11);
                SendMessage(_hWindow, (int) KeyState.KeyUp, (IntPtr) Key.Space, (IntPtr) 0x390000);
            }
            Thread.Sleep(1);
        }

        /// <summary>
        /// Thread for Jump Rock for tank.
        /// Jump and press M1 if key pressed.
        /// </summary>
        private void JumpRockThread()
        {
            if (IsKeyPressed(JumpRockKeyComboBox.SelectedKey))
            {
                Thread.Sleep(16);
                MouseEvent((int) KeyMouseHelper.MouseEvent.RightDown, 0, 0, 0, UIntPtr.Zero);
                Thread.Sleep(6);
                SendMessage(_hWindow, (int) KeyState.KeyDown, (IntPtr) Key.Space, (IntPtr) 0x390000);
                Thread.Sleep(10);
                MouseEvent((int) KeyMouseHelper.MouseEvent.RightUp, 0, 0, 0, UIntPtr.Zero);
                Thread.Sleep(10);
                SendMessage(_hWindow, (int) KeyState.KeyUp, (IntPtr) Key.Space, (IntPtr) 0x390000);
            }
            Thread.Sleep(1);
        }

        /// <summary>
        /// Thread for Crazy mouse.
        /// Randomly move mouse if key pressed.
        /// </summary>
        private void CrazyMouseThread()
        {
            if (IsKeyPressed(CrazyMouseKeyComboBox.SelectedKey))
            {
                int toX = _random.Next(0, Convert.ToInt32(SystemParameters.PrimaryScreenWidth));
                //int toY = _random.Next(0, Convert.ToInt32(SystemParameters.PrimaryScreenHeight));

                int screenWidth = GetSystemMetrics(0);
                //int screenHeight = GetSystemMetrics(1);

                // Mickey X coordinate
                uint micX = (uint) Math.Round(toX * 65536.0 / screenWidth);
                // Mickey Y coordinate
                //uint micY = (uint)Math.Round(toY * 65536.0 / screenHeight);

                // 0x0001 | 0x8000: Move + Absolute position
                // Set Y to 0 (top screen)
                MouseEvent(0x0001 | 0x8000, micX, dy: 0, dwData: 0, dwExtraInfo: UIntPtr.Zero);
            }
            Thread.Sleep(1);
        }

        #endregion

        #region Window

        private void GameComboBox_OnSelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            ComboBoxItem comboItem = GameComboBox.SelectedItem as ComboBoxItem;
            string gameName = comboItem?.Name;

            Game game = _games.First(x => x.Value == gameName);

            if (game == null) return;
            _gameWindowTitle = game.WindowTitle;
            RedrawControls(game);
        }

        private void RedrawControls(Game game)
        {
            if (KeysForScripts == null) return;

            foreach (object child in KeysForScripts.Children)
            {
                string childname = null;
                if (child is FrameworkElement)
                {
                    childname = (child as FrameworkElement).Name;
                }

                if (childname == null) continue;
                Script script = (Script)Enum.Parse(typeof(Script), childname);

                (child as FrameworkElement).Visibility = game.AvailableScrtips.HasFlag(script) ? Visibility.Visible : Visibility.Collapsed;
            }
        }

        public event PropertyChangedEventHandler PropertyChanged;

        private void OnPropertyChanged([CallerMemberName] string propertyName = null)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }

        protected override void OnClosed(EventArgs e)
        {
            _threads.Values.ToList().ForEach(t => t.Stop());
            base.OnClosed(e);
            Application.Current.Shutdown();
        }

        #endregion

        #region Interops

        [DllImport("user32.dll", SetLastError = true, CallingConvention = CallingConvention.Winapi)]
        private static extern IntPtr FindWindow(string lpClassName, string lpWindowName);

        [DllImport("user32.dll", CharSet = CharSet.Auto, CallingConvention = CallingConvention.Winapi)]
        private static extern IntPtr SendMessage(IntPtr hWnd, uint msg, IntPtr wParam, IntPtr lParam);

        [DllImport("user32.dll", EntryPoint = "mouse_event", CallingConvention = CallingConvention.Winapi)]
        private static extern void MouseEvent(uint dwFlags, uint dx, uint dy, uint dwData, UIntPtr dwExtraInfo);

        [DllImport("user32.dll", CallingConvention = CallingConvention.Winapi)]
        private static extern int GetSystemMetrics(int value);

        #endregion

    }
}

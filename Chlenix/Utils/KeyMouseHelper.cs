using System;
using System.Runtime.InteropServices;

namespace Chlenix.Utils
{
    public static class KeyMouseHelper
    {
        public enum Key
        {
            Space    = 0x20,
            LeftAlt  = 0x12,
            RButton  = 0x02,
            XButton1 = 0x05,
            XButton2 = 0x06,

            N        = 0x4E
        }

        public enum KeyState
        {
            KeyDown = 0x0100,
            KeyUp   = 0x0101
        }

        public enum MouseEvent
        {
            RightDown = 0x0008,
            RightUp   = 0x0010
        }

        [DllImport("user32.dll")]
        private static extern ushort GetAsyncKeyState(int vKey);

        /// <summary>
        /// Get key pressed status.
        /// </summary>
        /// <param name="key"></param>
        /// <returns>Return true if key is pressed and false if not</returns>
        public static bool IsKeyPressed(Key key)
        {
            bool result = (GetAsyncKeyState((int)key) & 0x8000) > 0;

            #if DEBUG
            if(result) Console.WriteLine($@"{key:G} Key Pressed");
            #endif

            return result;
        }
    }
}

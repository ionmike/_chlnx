﻿using System;

namespace Chlenix.Utils
{
    [Flags]
    internal enum Script
    {
        Bhop = 1 << 0,
        JumpRock = 1 << 1,
        CrazyMouse = 1 << 2
    }
}
